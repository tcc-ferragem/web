import React, { Component } from 'react'
import axios from 'axios'
import ProdutoLista from './services/ProdutoLista'
import Header from './Header'

import "./css/table.css"
import { Label } from 'react-foundation'
export default class Prod_List extends Component {

    state = {
        products: [],
        total: 0,
        search: ''
    }

    fetchById = (id) => {
        
        try {
            axios.get(`https://controle-ferragem.herokuapp.com/products/${id}`)
                .then(res => {
                    this.setState({
                        products: this.state.products.filter(product => product.id !== id)
                    })
                })
        } catch (erro) {
            alert(`Erro: ${erro}`)
        }
        let prodId= this.props.match.params.id;
    }
    componentDidMount() {
        axios.get('https://controle-ferragem.herokuapp.com/products')
        .then(res => {
            const products = res.data
            
            this.setState({ products })
            
        })
    }

    updateSearch(e) {
        this.setState({search: e.target.value.substr(0, 20)})
    }


    deleteProduct = (id) => {

        alert("Deletando...")
        
        try {
            axios.delete(`https://controle-ferragem.herokuapp.com/products/${id}`)
                .then(res => {
                    this.setState({
                        products: this.state.products.filter(product => product.id !== id)
                    })
                })
        } catch (erro) {
            alert(`Erro: ${erro}`)
        }
    }

 

    render() {

        let filteredProducts = this.state.products.filter(product => {
                return product.description.indexOf(this.state.
                    search.toUpperCase()) !== -1 
                    
            }
        );

        
        return (

            <div className="content">
                <Header/>
                <div id="listra">
                <h1 id="t_listra">Listagem de Produtos</h1>
                </div>
                <div></div>
                <label id="lpsearch">Pesquisar</label><br></br>
                <input type="text" id="psearch" value={this.state.search.toUpperCase()} onChange={this.updateSearch.bind(this)}/> 
                <div id="PTable">     
                
                <table className="rTable">
                
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Produto</th>
                        <th>Valor</th>
                        <th>Quant.</th>
                        <th>Tipo</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody> 
                    {filteredProducts.map((product) => (
                    <ProdutoLista
                    key={product.id}
                    id={product.id}
                    name={product.name}
                    description={product.description}
                    selling_price={product.selling_price}
                    quantity={product.quantity}
                    type={product.type}
                    excluir={() => this.deleteProduct(product.id)}
                    alter={product.id}
                    ></ProdutoLista>
                    ))}
                </tbody>
                </table>

                </div>
                
            </div>
        )
    }
}
