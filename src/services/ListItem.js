import React from 'react'
import { Link } from 'react-router-dom'


const ListItem = props => (
    <tr>
        <td>{props.pdcod}</td>
        <td>{props.pdqnt}</td>
        <td>{Number(props.pdprice).toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})}</td>
        <td>{Number(props.pdpriceTotal).toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})}</td>
        <td>   <button type="button" onClick={(e) => window.confirm(`Deseja excluir  + ${props.pdcod}` && props.excluir(props.id, e))}><i className="fas fa-times"></i></button></td>
    </tr>
)

export default ListItem