import React from 'react'
import { Link } from 'react-router-dom'


const ProdutoLista = props => (
    <tr>
        <td>{props.name}</td>
        <td>{props.description}</td>
        <td>{Number(props.selling_price).toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})}</td>
        <td>{props.quantity}</td>
        <td>{props.type}</td>
        <td>
            <button type="button" onClick={(e) => window.confirm(`Deseja excluir  + ${props.description}` && props.excluir(props.id, e))}><i className="fas fa-times"></i></button>
         <Link  to={`/alterProd/${props.id}`}><button type="button" ><i className="fas fa-edit"></i></button></Link>
        </td>
    </tr>
)

export default ProdutoLista