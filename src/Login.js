import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import './css/login.css'
import api from "./services/api"
import { login } from "./services/auth"
import styled from 'styled-components'
class Login extends Component {
state = {
    email: "",
    password: "",
    error: ""
}

handleSignIn = async e => {
    e.preventDefault()

    const { email, password} = this.state
    if(!email || !password || email === "" && password === ""){
        this.setState({ error: "Preencha os dados corretamente!"})
        alert(this.state.error)
    } else {
        try{
            const response = await api.post("/sessions", {email, password})
            login(response.data.token)
            this.props.history.push("/home")
        }catch(err) {
            this.setState({
                error:
                "Houve um problema, verifique as credenciais!"
            })
            alert(this.state.error)
        }
    }
}
    render() {
       
        const Button = styled.button`
        background: ${props => props.primary ?
            "black": "#4A36E2"};
            color: ${props => props.primary ? "white" : "white"};
    
            margin-left: 175px;
            padding: 14px 20px;
            font-family: 'IBM Plex Sans Medium';
            font-size: 20px;
            text-align: center;
            text-size-adjust: auto;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            margin-top: 25px;
            width: 300px;
            height: 50px;
            `;
        return (

            <div>
                
                <h1>Controle de Estoque</h1>
                <div id='tela'>
            <h3 id="loginh3">Login</h3>
            <div id='login'>
            <form onSubmit= {this.handleSignIn}>

            
            <input type='text' className='form-control' id='lemail' name='email' onChange={e => this.setState({ email: e.target.value})} placeholder='Usuário: ex.: Vinik@gmail.com'></input>
            <input type='password' className='form-control' id='lsenha' name='password' onChange={e => this.setState({ password: e.target.value})} placeholder='Senha'></input>
            <br></br>
            <button type="submit" id="loginSub">Entrar</button>
            </form>
            </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Login)
