import React, { Component } from 'react'
import Header from './Header'
import axios from 'axios'
import Chart from 'react-google-charts'
import { Bar } from 'react-chartjs-2';
import ProfitGraphic from './services/ProfitGraphic'
import './css/graphics.css'
export default class Graphics extends Component {

    state = {
        products: [],
        profit: '',
        data: [],
        totalProfit: ''
    }

    async componentDidMount() {
        axios.get('https://controle-ferragem.herokuapp.com/products')
            .then(res => {
                const products = res.data

                this.setState({ products })
                this.setState({ name: res.data })
            })

        axios.get('https://controle-ferragem.herokuapp.com/products')
            .then(res => {
                console.log(res)
                const ipl = res.data
                let prodname = []
                let buyprice = []
                let sellprice = []
                let profit = []
                let totalProfit= 0
                ipl.forEach(record => {
                    prodname.push(record.name)
                    buyprice.push(record.buying_price*record.quantity)
                    sellprice.push(record.selling_price*record.quantity)
                    profit.push((record.buying_price*record.quantity)-(record.selling_price*record.quantity))
                    totalProfit += (record.buying_price*record.quantity)-(record.selling_price*record.quantity)
                })
                this.setState({
                    Data: {
                        labels: prodname,
                        datasets: [{

                            label: 'Vendas',
                            data: sellprice,
                            backgroundColor: "Red",
                            hoverBackgroundColor: "Red"
                        },
                        {

                            label: 'Compras',
                            data: buyprice,
                            backgroundColor: "yellow",
                            hoverBackgroundColor: "yellow"
                        },
                        {
                            label: 'Lucro',
                            data: profit,
                            backgroundColor: "Green",
                            hoverBackgroundColor: "Green"
                        }
                        ]
                    },
                    totalProfit: totalProfit
                })
            })

    }
    render() {

        return (
            <div>
                <Header />
                <div id="listra">
                    <h1 id="t_listra">Gráficos</h1>
                </div>
                <div id="grafico">
                    <Bar data={this.state.Data} options={{ maintainAspectRatio: false }}></Bar>
                    <h2 id="tlucro">Lucros: {this.state.totalProfit}</h2>
                </div>
                
            </div>
        )
    }
}
