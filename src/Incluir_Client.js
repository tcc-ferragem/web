import React, { Component } from 'react'
import Header from './Header'
import './css/inc_cli.css'
import styled from 'styled-components'
import axios from 'axios'
export default class Incluir_Prod extends Component {

    state = {
        name: '',
        fantasy_name: '',
        cnpj: '',
        address: '',
        city: '',
        state: ''
    }


    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit = e => {
        e.preventDefault();

        const clients = {

            name: this.state.name,
            fantasy_name: this.state.fantasy_name,
            cnpj: this.state.cnpj,
            address: this.state.address,
            city: this.state.city,
            state: this.state.state
        }

        const message = window.confirm("Deseja incluir " + this.state.name + " na tabela de clientes?")

        if (message === true) {


            axios.post(`https://controle-ferragem.herokuapp.com/clients`, clients)
                .then(res => {
                    console.log(res);
                    console.log(res.data)
                })

            alert("Cliente cadastrado com sucesso!")

            this.setState({
                name: '',
                fantasy_name: '',
                cnpj: '',
                address: '',
                city: '',
                state: ''
            })
        } else {
            alert("Cliente não foi cadastrado, favor verificar os dados!")
        }
    }



    render() {
        const Button = styled.button`
        background: ${props => props.primary ?
                "black" : "#4A36E2"};
            color: ${props => props.primary ? "white" : "white"};
    
            margin-left: 175px;
            padding: 10px 20px;
            font-family: 'IBM Plex Sans Medium';
            font-size: 20px;
            text-align: center;
            text-size-adjust: auto;
            border: 3px solid #4A36E2;
            border-radius: 3px;
            cursor: pointer;
            margin-top: 25px;
            width: 300px;
            height: 50px;
            `;
        return (
            <div>
                <Header />
                <div id="listra">
                    <h1 id="t_listra">Incluir Cliente</h1>
                </div>
                <div id="pjumb">

                    <h3 id="ptext">Preencha todas as informações</h3>
                    <form onSubmit={this.handleSubmit} role="form" >
                        <label for="pname">
                            Nome:<br></br>
                        </label>
                        <input type="text" id="pname" name="name" onChange={this.handleChange} value={this.state.name} autoFocus />

                        <label for="pfantasy_name">Nome Fantasia:</label>
                        <input type="text" id="pfantasy_name" name="fantasy_name" onChange={this.handleChange} value={this.state.fantasy_name} />
                        <br></br>

                        <label for="pcnpj">
                            CNPJ:<br></br>
                        </label>
                        <input type="Number" id="pcnpj" name="cnpj" onChange={this.handleChange} value={this.state.cnpj} />

                        <label for="paddress">
                            Endereço:
                        </label>
                        <input type="text" id="paddress" name="address" onChange={this.handleChange} value={this.state.address} />
<br></br>
                        <label for="pcity">
                            Cidade:<br></br>
                        </label>
                        <input type="text" id="pcity" name="city" onChange={this.handleChange} value={this.state.city} />

                        <label for="pemail">
                            Email:
                        </label>
                        <input type="text" id="pemail" name="state" onChange={this.handleChange} value={this.state.state} />

                        <Button type="submit">Cadastrar Cliente</Button>

                    </form>

                </div>
            </div>
        )
    }
}
