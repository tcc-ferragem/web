import React, {component} from 'react';
import { BrowserRouter as Router, Route, Switch, useParams} from 'react-router-dom'
import Login from './Login'
import Incluir_Prod from './Incluir_Prod'
import Home from './Home'
import Prod_List from './Prod_List'
import Incluir_Client from './Incluir_Client'
import Incluir_Ped from './Incluir_Ped'
import AlterProd from './AlterProd'
import { isAuthenticated } from "./services/auth"
import { Redirect } from 'react-router-dom';
import Graphics from './Graphics';

function App() {
 
  const privateRoute = ({ component: Component, ...rest }) => (
    <Route 
      {...rest}
      render={props =>
      isAuthenticated() ? (
        <Component {...props} />
  
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
      }
      />
  )
     function User(){
       let { id } = useParams();

     }

  return (
    

    <Router>
      
      <Route path='/' exact component={Login} />
      
      <Switch>
      <Route path='/alterProd/:id'component={AlterProd}/>
      </Switch>
      <Route path='/graficos' component={Graphics}/>
      <Route path="/inPed" component={Incluir_Ped}/>
      <Route path='/inClient' component={Incluir_Client}/>
      <Route path='/home' component={Home} />
      <Route path='/inProd' component={Incluir_Prod} />
      <Route path='/PList' component={Prod_List}/>
    </Router>
  );
}

export default App;
