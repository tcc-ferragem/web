import React, { Component } from 'react'
import Header from './Header'
import './css/inc_prod.css'
import styled from 'styled-components'
import axios from 'axios'
export default class Incluir_Prod extends Component {

    state = {
        name: '',
        description: '',
        buying_price: 0.0,
        selling_price: 0.0,
        quantity: 0,
        type: ''
    }


    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit = e => {
        e.preventDefault();

        const products = {

            name: this.state.name.toUpperCase(),
            description: this.state.description.toUpperCase(),
            buying_price: this.state.buying_price,
            selling_price: this.state.selling_price,
            quantity: this.state.quantity,
            type: this.state.type.toUpperCase()
        }

        const message = window.confirm("Deseja incluir " + this.state.name + " do tipo: " + this.state.type + "na tabela de produtos?")

        if (message === true) {


            axios.post(`https://controle-ferragem.herokuapp.com/products`, products)
                .then(res => {
                    console.log(res);
                    console.log(res.data)
                })

            alert("Produto cadastrado com sucesso!")

            this.setState({
                name: '',
                description: '',
                buying_price: '',
                selling_price: '',
                quantity: '',
                type: ''
            })
        } else {
            alert("Produto não foi cadastrado, favor verificar os dados!")
        }
    }



    render() {
        const Button = styled.button`
        background: ${props => props.primary ?
                "black" : "#4A36E2"};
            color: ${props => props.primary ? "white" : "white"};
    
            margin-left: 175px;
            padding: 10px 20px;
            font-family: 'IBM Plex Sans Medium';
            font-size: 20px;
            text-align: center;
            text-size-adjust: auto;
            border: 3px solid #4A36E2;
            border-radius: 3px;
            cursor: pointer;
            margin-top: 25px;
            width: 300px;
            height: 50px;
            `;
        return (
            <div>
                <Header />
                <div id="listra">
                    <h1 id="t_listra">Incluir Produto</h1>
                </div>
                <div id="pjumb">

                    <h3 id="ptext">Preencha todas as informações</h3>
                    <form onSubmit={this.handleSubmit} role="form" >
                        <label for="pname">
                            Código:<br></br>
                        </label>
                        <input type="text" id="pname" name="name" onChange={this.handleChange} value={this.state.name} autoFocus />

                        <label for="pdescription">Descrição:</label>
                        <input type="text" id="pdescription" name="description" onChange={this.handleChange} value={this.state.description} />
                        <br></br>

                        <label for="pbuying_price">
                            Preço de compra:<br></br>
                        </label>
                        <input type="Number" id="pbuying_price" name="buying_price" onChange={this.handleChange} value={this.state.buying_price} />

                        <label for="pselling_price">
                            Preço de venda:
                        </label>
                        <input type="Number" id="pselling_price" name="selling_price" onChange={this.handleChange} value={this.state.selling_price} />

                        <label for="pquantity">
                            Quantidade em estoque:<br></br>
                        </label>
                        <input type="Number" id="pquantity" name="quantity" onChange={this.handleChange} value={this.state.quantity} />

                        <label for="ptype">
                            Tipo:
                        </label>
                        <input type="text" id="ptype" name="type" onChange={this.handleChange} value={this.state.type} />

                        <Button type="submit">Cadastrar Produtos</Button>

                    </form>

                </div>
            </div>
        )
    }
}
