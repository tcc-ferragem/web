import React, { Component, useState } from 'react'
import axios from 'axios'
import { debounce } from 'lodash'
import ReactAutocomplete from 'react-autocomplete'
import styled, { css } from 'styled-components'
import ListItem from './services/ListItem'
import Header from './Header'
import { Redirect } from 'react-router-dom'
import "./css/inc_ped.css"


export default class Incluir_Ped extends Component {

    state = {
        item_id: 0,
        itens: [],
        pdcod: '',
        pdprice: '',
        pdqnt: '',
        products: [],
        results: [],
        clients: [],
        client: '',
        product_id: '',
        client_id: '',
        value: ''
    }
    async componentDidMount() {
        axios.get('https://controle-ferragem.herokuapp.com/products')
            .then(res => {
                const products = res.data

                this.setState({ products })

            })
        axios.get('https://controle-ferragem.herokuapp.com/clients')
            .then(res => {
                const clients = res.data
                this.setState({ clients })
            })





    }
    handleChange = e => {

        this.setState({ [e.target.name]: e.target.value })

    }

    handleChangeProd(value, id) {
        console.log("value>>>", value)
        console.log("id>>>", id)
    }
    deleteItem = (id) => {

        alert("Deletando...")

        try {
            const items = this.state.itens.filter(item => item.id !== id)
            this.setState({ itens: items })
        } catch (erro) {
            alert(`Erro: ${erro}`)
        }

    }

    handleAddItem = e => {
        e.preventDefault()


        const item = {
            id: this.state.item_id++,
            pdcod: this.state.value.toUpperCase(),
            pdqnt: this.state.pdqnt,
            pdprice: this.state.pdprice,
            pdpriceTotal: this.state.pdprice * this.state.pdqnt
        }

        let joined = this.state.itens.concat(item)
        this.setState({ itens: joined })

        this.setState({
            pdcod: '',
            pdqnt: '',
            pdprice: '',
            pdpriceTotal: 0,
            value: '',
        })

    }

    async componentWillMount() {
        const id = this.state.product_id
        const url = `https://controle-ferragem.herokuapp.com/products/${id}`
        const response = await fetch(url)
        const data = await response.json()
        console.log(data)
    }

    handleSubmit = e => {
        e.preventDefault();

        const orders = {

            client: this.state.client,
            itens: this.state.itens,
        
        }

        const message = window.confirm("Deseja incluir o pedido para " + this.state.client + " na tabela de pedidos?")

        if (message === true) {


            axios.post(`https://controle-ferragem.herokuapp.com/orders`, orders)
                .then(res => {
                    console.log(res);
                    console.log(res.data)
                })

            alert("Pedido cadastrado com sucesso!")
            return  <Redirect to="/PList"/>
          
        } else {
            alert("Produto não foi cadastrado, favor verificar os dados!")
        }
    }
    render() {
        const {redirect} = this.state;

        if(redirect) {
            return <Redirect to='/PList'/>
        }
        const Button = styled.button`
        background: ${props => props.primary ?
                "black" : "#4A36E2"};
            color: ${props => props.primary ? "white" : "white"};
    
            margin-left: 175px;
            padding: 5px 15px;
            font-family: 'IBM Plex Sans Medium';
            font-size: 20px;
            text-align: center;
            text-size-adjust: auto;
            border: 3px solid #4A36E2;
            border-radius: 3px;
            cursor: pointer;
            margin-top: 25px;
            width: 150px;
            height: 40px;
            `;


        return (
            <div>
                <Header />
                <div id="listra">
                    <h1 id="t_listra">Incluir Pedido</h1>
                </div>
                <div id="pdjumb">

                    <div>
                        <label for="pcliente">Cliente</label>
                        <ReactAutocomplete
                            id="pcliente"
                            items={this.state.clients}
                            shouldItemRender={(item, pcliente) => item.name.toUpperCase().indexOf(pcliente.toUpperCase()) > -1}
                            getItemValue={item => item.name}
                            renderItem={(item, highlighted, ) =>
                                <div
                                    key={item.id}
                                    style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}
                                >
                                    {item.name}

                                </div>
                            }
                            value={this.state.client}
                            onChange={this.handleChange}
                            onSelect={(client, item) => this.setState({ client, cliente_id: item.id })}
                        />
                        <label for="ppcnpj">CNPJ</label>
                        <input type="text" id="ppcnpj" name="pcnpj" onChange={this.handleChange}></input>
                    </div>
                    <div>
                        <h3 id="pedProd">Produtos</h3>
                        <div id="listrinha"></div>
                    </div>
                    <div>
                        <form onSubmit={this.handleAddItem}>
                            <label for="pdcod">Código</label>
                            <ReactAutocomplete
                                id="pdcod"
                                items={this.state.products}
                                shouldItemRender={(item, pdcod) => item.name.toUpperCase().indexOf(pdcod.toUpperCase()) > -1}
                                getItemValue={item => item.name}
                                renderItem={(item, highlighted, ) =>
                                    <div
                                        key={item.id}
                                        style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}
                                    >
                                        {item.name}

                                    </div>
                                }
                                value={this.state.value}
                                onChange={this.handleChange}
                                onSelect={(value, item) => this.setState({ value, product_id: item.id, pdprice: item.selling_price })}
                            />
                            <label for="pdqnt">Quant.</label>
                            <input type="Number" id="pdqnt" name="pdqnt" value={this.state.pdqnt} onChange={this.handleChange}></input>

                            <label for="pdprice">Preço</label>
                            <input type="Currency" id="pdprice" name="pdprice" value={this.state.pdprice.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })} onChange={this.handleChange}></input>

                            <Button type="submit" id="sub">Incluir</Button>
                        </form>
                        <form onSubmit={this.handleSubmit}>
                        <div id="PTable">
                            
                                <table className="rTable">

                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Quantidade</th>
                                            <th>Valor</th>
                                            <th>Valor Total</th>
                                            <th>Excluir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.itens.map((item) =>
                                            <ListItem
                                                key={item.id}
                                                pdcod={item.pdcod}
                                                pdqnt={item.pdqnt}
                                                pdprice={item.pdprice}
                                                pdpriceTotal={item.pdpriceTotal}
                                                excluir={() => this.deleteItem(item.id)}
                                            ></ListItem>)}
                                    </tbody>
                                </table>
                              
                        </div>
                        
                                <Button type="submit" id="gravar">always</Button>
                            </form>
                    </div>
                </div>
            </div>
        )
    }
}
