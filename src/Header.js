import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import './css/header.css'

export default class Header extends Component {
    render() {

        const Button = styled.button`
    background: ${props => props.primary ?
        "#4A36E2": "white"};
        color: ${props => props.primary ? "white" : "#4A36E2"};

        font-size: 20px;
        font-family: "IBM Plex Sans Medium";
        margin: 25px;
        padding: 0em 0em;
        border: 3px solid #4A36E2;
        border-radius: 3px;
        width: 160px;
        height: 50px;
        `;


        return (
        <header>
<ul className="nav-flex-column">
    <li className="nav-item">
    <Link to='/home' id="logo">VK</Link>
    </li>       
    <li className="nav-item">
    <Link to='/inPed'><Button primary>Incluir Pedido</Button></Link>
    </li>
    <li className="nav-item">
      <Link to='/inClient'><Button primary>Incluir Cliente</Button></Link>
    </li>
    <li className="nav-item">
      <Link to='/inProd'><Button primary>Incluir Produto</Button></Link>
    </li>
    <li className="nav-item">
        <Link to='/PList'><Button primary>Listagem</Button></Link>
    </li>
    <li className="nav-item">
        <Link to='/graficos'><Button primary>Gráficos</Button></Link>
    </li>
  </ul>
 
  </header>
                
        )
    }
}
